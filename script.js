
let mouseDown = false
document.body.onmousedown = () => (mouseDown = true)
document.body.onmouseup = () => (mouseDown = false)


function createSquares(newSquareAmount) {

  if (newSquareAmount == undefined) {
    newSquareAmount = 8
  } if (newSquareAmount < 1){
    newSquareAmount = 2
  }


  let gridsize = newSquareAmount
  const container = document.querySelector('#container');

  for (i = 0; i < gridsize * gridsize; i++) {
    const div = document.createElement("div");
    div.classList.add('box');
    div.setAttribute("id", i);
    div.style.width = container.offsetWidth/gridsize
    container.appendChild(div);

  }
}


function resetSquares() {
  newSquareAmount = prompt("How many squares");

  if (newSquareAmount > 32) {
    alert("too many squares")
    return;
  }

  const element = document.getElementById("container");
  element.remove(); // Removes the div with the 'div-02' id

  const div = document.createElement("div");
  div.setAttribute("id", "container");
  field.appendChild(div);
  
  createSquares(newSquareAmount)
  addHover()
}

function addHover() {

const boxes = document.querySelectorAll('.box');


boxes.forEach((box) => {

  box.addEventListener('mouseover', () => {
    box.classList.add('hover');
      if (mouseDown) {
        box.classList.add('clicked');
      }
  });

  box.addEventListener('mouseout', () => {
    box.classList.remove('hover');
  });

  box.addEventListener('mousedown', () => {
    box.classList.add('clicked');
  });



});
}


createSquares()
addHover()